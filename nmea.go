// Package nmea reads and writes NMEA 0183 formatted sentences.
//
// Reference: http://en.wikipedia.org/wiki/NMEA_0183
package nmea

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

var (
	ErrSentence = errors.New("Bad sentence format")
	ErrChecksum = errors.New("Invalid checksum")
)

type Sentence struct {
	// Sender ID and message type
	Id string
	// Message fields
	Fields []string
}

// IsEmpty returns true if the Sentence has no content
func (s Sentence) IsEmpty() bool {
	return s.Id == ""
}

// UnmarshalText implements the encoding.TextUnmarshaler interface.
func (s *Sentence) UnmarshalText(data []byte) error {
	i := bytes.IndexByte(data, '$')
	if i == -1 {
		return fmt.Errorf("%q: %w", string(data), ErrSentence)
	}

	fields := bytes.Split(bytes.TrimRight(data[i+1:], "\r\n"), []byte(","))
	n := len(fields)
	// Split the checksum from the last field
	tail := bytes.Split(fields[n-1], []byte("*"))
	fields[n-1] = tail[0]
	if len(tail) == 2 && len(tail[1]) == 0 {
		return fmt.Errorf("%q: %w", string(data), ErrSentence)
	}

	s.Fields = make([]string, 0, n-1)
	for j, f := range fields {
		if j == 0 {
			s.Id = string(f)
		} else {
			s.Fields = append(s.Fields, string(f))
		}
	}

	// Checksum present
	if len(tail) == 2 {
		c := s.CalcCsum()
		csum, err := strconv.ParseUint(string(tail[1][0:2]), 16, 8)
		if err != nil {
			return fmt.Errorf("%q: %w", string(data), err)
		}

		if c != byte(csum) {
			return fmt.Errorf("%q: %w", string(data), ErrChecksum)
		}
	}

	return nil
}

// ParseSentence returns a new Sentence parsed from a byte slice.
func ParseSentence(data []byte) (Sentence, error) {
	s := &Sentence{}
	err := s.UnmarshalText(data)
	return *s, err
}

// ReadSentence returns the next Sentence from an io.Reader
func ReadSentence(r io.Reader) (Sentence, error) {
	return ReadSentenceCtx(context.Background(), r)
}

// ReadSentenceCtx returns the next Sentence from an io.Reader returning when the
// sentence is read or ctx is canceled.
func ReadSentenceCtx(ctx context.Context, r io.Reader) (Sentence, error) {
	char := make([]byte, 1)
	rec := make([]byte, 0, 128)
	bcsum := make([]byte, 0, 2)
	s := Sentence{Fields: []string{}}

	const startChar = '$'
	const endChar = '\n'

	for {
		select {
		case <-ctx.Done():
			return s, ctx.Err()
		default:
		}
		n, err := r.Read(char)
		if n == 0 {
			err = os.ErrDeadlineExceeded
		}
		if err != nil {
			return s, fmt.Errorf("looking for START: %w", err)
		}
		if char[0] == startChar {
			break
		}
	}

	csum := byte(0)
	hasCsum := false

	for {
		select {
		case <-ctx.Done():
			return s, ctx.Err()
		default:
		}

		n, err := r.Read(char)
		if n == 0 {
			err = os.ErrDeadlineExceeded
		}
		if err != nil {
			return s, fmt.Errorf("looking for END: %w", err)
		}

		if char[0] == endChar {
			break
		}

		if char[0] == '*' {
			hasCsum = true
			continue
		}

		if hasCsum {
			bcsum = append(bcsum, char[0])
		} else {
			rec = append(rec, char[0])
			csum = csum ^ char[0]
		}
	}

	var (
		err      error
		sentCsum uint64
	)

	switch len(bcsum) {
	case 3, 2:
		sentCsum, err = strconv.ParseUint(string(bcsum[0:2]), 16, 8)
		if err != nil {
			err = fmt.Errorf("Bad checksum syntax: %w", err)
		}
		if uint8(sentCsum) != csum {
			err = ErrChecksum
		}
	case 1:
		err = fmt.Errorf("Invalid checksum")
	}

	fields := bytes.Split(bytes.TrimRight(rec, "\r\n"), []byte(","))
	n := len(fields)
	s.Fields = make([]string, 0, n-1)
	s.Id = string(fields[0])
	for _, f := range fields[1:] {
		s.Fields = append(s.Fields, string(f))
	}

	return s, err
}

// CalcCsum calculates the NMEA sentence checksum.
func (s Sentence) CalcCsum() byte {
	csum := byte(0)
	for _, c := range s.Id {
		csum = csum ^ byte(c)
	}

	for _, f := range s.Fields {
		for _, c := range f {
			csum = csum ^ byte(c)
		}
		// The extra ',' incorporated here makes-up for
		// the one skipped between the Id and the first
		// Field and it saves a comparison op.
		csum = csum ^ byte(',')
	}

	return csum
}

// String implements the fmt.Stringer interface
func (s Sentence) String() string {
	csum := s.CalcCsum()
	if len(s.Fields) == 0 {
		return fmt.Sprintf("$%s*%02X", s.Id, csum)
	}
	return fmt.Sprintf("$%s,%s*%02X", s.Id,
		strings.Join(s.Fields, ","), csum)
}

// MarshalText implements the encoding.TextMarshaler interface.
func (s Sentence) MarshalText() ([]byte, error) {
	text := s.String()
	return []byte(text), nil
}
