package nmea

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

// NMEAUnmarshaler is the interface implemented by an object that can unmarshal
// itself from an NMEA sentence.
type NMEAUnmarshaler interface {
	UnmarshalNMEA(s Sentence) error
}

// NMEAMarshaler is the interface implemented by an object that can marshal itself
// into an NMEA sentence.
type NMEAMarshaler interface {
	MarshalNMEA() (Sentence, error)
}

// Set a reflect.Value from the contents of the string src. Only integers
// (signed and unsigned), floats, and strings are supported. If the value
// is not changeable, this function will panic.
func setValue(v reflect.Value, src string, base int) error {
	switch v.Kind() {
	case reflect.Uint8:
		x, err := strconv.ParseUint(src, base, 8)
		if err != nil {
			return err
		}
		v.SetUint(x)
	case reflect.Uint16:
		x, err := strconv.ParseUint(src, base, 16)
		if err != nil {
			return err
		}
		v.SetUint(x)
	case reflect.Uint32:
		x, err := strconv.ParseUint(src, base, 32)
		if err != nil {
			return err
		}
		v.SetUint(x)
	case reflect.Uint64:
		x, err := strconv.ParseUint(src, base, 64)
		if err != nil {
			return err
		}
		v.SetUint(x)
	case reflect.Int8:
		x, err := strconv.ParseInt(src, base, 8)
		if err != nil {
			return err
		}
		v.SetInt(x)
	case reflect.Int16:
		x, err := strconv.ParseInt(src, base, 16)
		if err != nil {
			return err
		}
		v.SetInt(x)
	case reflect.Int32:
		x, err := strconv.ParseInt(src, base, 32)
		if err != nil {
			return err
		}
		v.SetInt(x)
	case reflect.Int64:
		x, err := strconv.ParseInt(src, base, 64)
		if err != nil {
			return err
		}
		v.SetInt(x)
	case reflect.Int:
		x, err := strconv.ParseInt(src, base, 64)
		if err != nil {
			return err
		}
		v.SetInt(x)
	case reflect.Float32, reflect.Float64:
		x, err := strconv.ParseFloat(src, 64)
		if err != nil {
			return err
		}
		v.SetFloat(x)
	case reflect.String:
		v.SetString(src)
	default:
		return fmt.Errorf("Unsupported type: %s", v.Kind())
	}
	return nil
}

// UnmarshalNMEA reads the contents of a byte slice into the struct
// pointed to by v. Only exported fields of the struct are filled.
func UnmarshalNMEA(b []byte, v interface{}) error {
	s := Sentence{}
	p := &s
	err := p.UnmarshalText(b)
	if err != nil {
		return err
	}

	if um, ok := v.(NMEAUnmarshaler); ok {
		return um.UnmarshalNMEA(s)
	}

	val := reflect.ValueOf(v)

	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}

	if val.Kind() == reflect.Interface {
		val = reflect.ValueOf(val.Interface())
	}

	if val.Kind() != reflect.Struct {
		return errors.New("Can only unmarshal into structs")
	}

	var (
		base int
	)

	j := int(0)
	nf := len(s.Fields)

	for i := 0; i < val.NumField(); i++ {
		if fv := val.Field(i); fv.CanSet() {
			t := val.Type()
			if t.Field(i).Name == "NMEAName" {
				setValue(fv, s.Id, 0)
				continue
			}
			if fv.Kind() == reflect.Array {
				// The presence of the "hex" tag means the values are
				// hex encoded integers.
				if _, ok := t.Field(i).Tag.Lookup("hex"); ok {
					base = 16
				} else {
					base = 10
				}
				for k := 0; k < fv.Len(); k++ {
					if j >= nf {
						return fmt.Errorf("Invalid NMEA for %T: %q", v, s)
					}
					err = setValue(fv.Index(k), s.Fields[j], base)
					j++
				}
			} else {
				if j >= nf {
					return fmt.Errorf("Invalid NMEA for %T: %q", v, s)
				}
				err = setValue(fv, s.Fields[j], 10)
				j++
			}

			if err != nil {
				return fmt.Errorf("Cannot set field %d: %w", i, err)
			}
		}
	}

	return nil
}

// Convert a reflect.Value to a string. Only integers (signed and
// unsigned), floats, and strings are supported.
func getValueAsString(v reflect.Value) (string, error) {
	switch v.Kind() {
	case reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uint:
		return strconv.FormatUint(v.Uint(), 10), nil
	case reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64, reflect.Int:
		return strconv.FormatInt(v.Int(), 10), nil
	case reflect.Float32:
		return strconv.FormatFloat(v.Float(), 'f', -1, 32), nil
	case reflect.Float64:
		return strconv.FormatFloat(v.Float(), 'f', -1, 64), nil
	case reflect.String:
		return v.String(), nil
	}
	return "", fmt.Errorf("Unsupported type: %s", v.Kind())
}

// Marshal writes the contents of the struct v as an NMEA sentence. The sentence ID
// is obtained from , in order of preference:
//
//    - the value of the NMEAName field of type string
//    - the upper-case name of the marshaled type
//
// Only exported fields are included in the sentence.
func MarshalNMEA(v interface{}) ([]byte, error) {
	if m, ok := v.(NMEAMarshaler); ok {
		s, err := m.MarshalNMEA()
		if err != nil {
			return nil, err
		}
		return s.MarshalText()
	}

	val := reflect.ValueOf(v)
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}

	if val.Kind() != reflect.Struct {
		return nil, errors.New("Can only marshal from structs")
	}

	var s_id string
	t := val.Type()

	if nv := val.FieldByName("NMEAName"); nv.IsValid() && nv.Kind() == reflect.String {
		s_id = nv.String()
	} else {
		s_id = strings.ToUpper(t.Name())
	}

	s := Sentence{Id: s_id}
	s.Fields = make([]string, 0, val.NumField())

	for i := 0; i < val.NumField(); i++ {
		if fv := val.Field(i); fv.CanInterface() {
			if t.Field(i).Name == "NMEAName" {
				continue
			}
			if format, ok := t.Field(i).Tag.Lookup("fmt"); ok {
				s.Fields = append(s.Fields, fmt.Sprintf(format, fv.Interface()))
			} else {
				sval, _ := getValueAsString(fv)
				s.Fields = append(s.Fields, sval)
			}
		}
	}

	return s.MarshalText()
}
