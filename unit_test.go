package nmea

import (
	"bufio"
	"reflect"
	"strings"
	"testing"
)

func TestNmeaParse(t *testing.T) {
	line := []byte("$PRVAT,00.115,M,0010.073,dBar*39\r\n")
	s, err := ParseSentence(line)
	if err != nil {
		t.Error(err)
	}

	if s.Id != "PRVAT" {
		t.Errorf("Bad NMEA sentence ID: %s", s.Id)
	}

	if len(s.Fields) != 4 {
		t.Errorf("Wrong NMEA field count: %d", len(s.Fields))
	}

	// Missing checksum is ok
	line = []byte("$PRVAT,00.115,M,0010.073,dBar\r\n")
	s, err = ParseSentence(line)
	if err != nil {
		t.Error(err)
	}

	if len(s.Fields) != 4 {
		t.Errorf("Wrong NMEA field count: %d", len(s.Fields))
	}

	if s.Fields[3] != "dBar" {
		t.Errorf("Bad value for last field: %q", s.Fields[3])
	}

	// This is not ok ...
	line = []byte("$PRVAT,00.115,M,0010.073,dBar*")
	s, err = ParseSentence(line)
	if err == nil {
		t.Error("Bad NMEA format not detected")
	}

	// Characters before '$' should be ignored
	line = []byte("blah blah blah\t$PRVAT,00.115,M,0010.073,dBar")
	s, err = ParseSentence(line)
	if err != nil {
		t.Error(err)
	}

	// Check that empty fields are properly parsed
	line = []byte("$GPGSA,A,3,10,07,05,02,29,04,08,13,,,,,1.72,1.03,1.38*0A")
	s, err = ParseSentence(line)
	if err != nil {
		t.Error(err)
	}

	if len(s.Fields) != 17 {
		t.Errorf("Wrong NMEA field count: %d", len(s.Fields))
	}
}

func TestNmeaWrite(t *testing.T) {
	example := "$GPAAM,A,A,0.10,N,WPTNME*32"
	s := Sentence{}
	s.Id = "GPAAM"
	s.Fields = []string{"A", "A", "0.10", "N", "WPTNME"}
	csum := s.CalcCsum()
	if csum != byte(0x32) {
		t.Errorf("Bad checksum calculation: %02X", csum)
	}
	if s.String() != example {
		t.Errorf("Bad output format: %q", s.String())
	}
}

type adata struct {
	NMEAName string
	Ival     int
	fval     float32
	Sval     string
}

type bdata struct {
	Ival int
	Fval float32 `fmt:"%.2f"`
	Sval string
}

type cdata struct {
	NMEAName string
}

func TestMarshal(t *testing.T) {
	table := []struct {
		in  interface{}
		out string
	}{
		{
			in: adata{
				NMEAName: "XDATA",
				Ival:     42,
				Sval:     "hello",
			},
			out: "$XDATA,42,hello*2C",
		},
		{
			in: bdata{
				Ival: 101,
				Fval: 3.14159,
				Sval: "bye",
			},
			out: "$BDATA,101,3.14,bye*28",
		},
		{
			in:  cdata{NMEAName: "CDATA"},
			out: "$CDATA*53",
		},
	}

	for _, e := range table {
		out, err := MarshalNMEA(e.in)
		if err != nil {
			t.Fatal(err)
		}

		if string(out) != e.out {
			t.Errorf("Bad output; expected %q, got %q", e.out, out)
		}
	}

}

func TestUnmarshal(t *testing.T) {
	table := []struct {
		in         string
		out, check interface{}
	}{
		{
			in: "$XDATA,42,hello*2C",
			out: &adata{
				NMEAName: "XDATA",
				Ival:     42,
				Sval:     "hello",
			},
			check: &adata{},
		},
		{
			in: "$BDATA,101,3.14,bye*28",
			out: &bdata{
				Ival: 101,
				Fval: 3.14,
				Sval: "bye",
			},
			check: &bdata{},
		},
		{
			in:    "$CDATA*53",
			out:   &cdata{NMEAName: "CDATA"},
			check: &cdata{},
		},
	}

	for _, e := range table {
		err := UnmarshalNMEA([]byte(e.in), e.check)
		if err != nil {
			t.Fatal(err)
		}

		if !reflect.DeepEqual(e.out, e.check) {
			t.Errorf("Bad output; expected %#v, got %#v", e.out, e.check)
		}
	}

}

func TestRead(t *testing.T) {
	table := []struct {
		in  string
		out Sentence
		err error
	}{
		{
			in:  "$HCHDG,356.3,0.0,W,0.0,W*41\r\n",
			out: Sentence{Id: "HCHDG", Fields: []string{"356.3", "0.0", "W", "0.0", "W"}},
		},
		{
			in:  "$HCHDG,356.3,0.0,W,0.0,W*41\n",
			out: Sentence{Id: "HCHDG", Fields: []string{"356.3", "0.0", "W", "0.0", "W"}},
		},
		{
			in:  "$HCHDG,356.3,0.0,W,0.0,W\r\n",
			out: Sentence{Id: "HCHDG", Fields: []string{"356.3", "0.0", "W", "0.0", "W"}},
		},
		{
			in:  "$HCHDG,356.3,0.0,W,0.0,W*42\r\n",
			out: Sentence{Id: "HCHDG", Fields: []string{"356.3", "0.0", "W", "0.0", "W"}},
			err: ErrChecksum,
		},
	}

	for _, e := range table {
		rdr := strings.NewReader(e.in)
		s, err := ReadSentence(rdr)
		if err != e.err {
			t.Errorf("Expected %v, got %v", e.err, err)
		}
		if !reflect.DeepEqual(s, e.out) {
			t.Errorf("Bad value; expected %#v, got %#v", e.out, s)
		}
	}
}

func BenchmarkReader(b *testing.B) {
	in := "$HCHDG,356.3,0.0,W,0.0,W*41\r\n"
	rdr := strings.NewReader(in)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ReadSentence(rdr)
		rdr.Reset(in)
	}
}

func BenchmarkParser(b *testing.B) {
	in := "$HCHDG,356.3,0.0,W,0.0,W*41\r\n"
	s := strings.NewReader(in)
	rdr := bufio.NewReader(s)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf, _ := rdr.ReadBytes('\n')
		ParseSentence(buf)
		s.Reset(in)
		rdr.Reset(s)
	}
}
